package com.assignment5.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

import com.assignment5.activities.AddFragment;
import com.assignment5.activities.EditFragment;
import com.assignment5.activities.StudentListFragment;
import com.assignment5.activities.ViewFragment;

/**
 * Created by click on 7/15/15.
 */
public class PagerAdapter extends FragmentPagerAdapter {
    final String TAB_TITLE_1="Students";
    final String TAB_TITLE_2="Add Student";
    final String TAB_TITLE_3="Student Information";
    final String TAB_TITLE_4="Edit details";

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;
        if(position==0){
            fragment=new StudentListFragment();
            return fragment;
        }
        if(position==1){
            fragment=new AddFragment();
            return fragment;
        }
        if(position==2){
            fragment=new ViewFragment();
            return fragment;
        }
        if(position==3){
            fragment=new EditFragment();
           //fragment.getView().setVisibility(View.INVISIBLE);
            return fragment;
        }
    return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position==0){
            return TAB_TITLE_1;
        }
        if(position==1){
            return TAB_TITLE_2;
        }
        if(position==2){
            return TAB_TITLE_3;
        }
        if(position==3){

            return TAB_TITLE_4;
        }
    return null;
    }


}
