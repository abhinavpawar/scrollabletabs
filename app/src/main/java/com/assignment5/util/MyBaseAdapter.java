package com.assignment5.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.assignment5.activities.R;
import com.assignment5.util.Student;

import java.util.ArrayList;

public class MyBaseAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private ArrayList<Student> mStudent;

    public MyBaseAdapter(Context context, ArrayList<Student> student) {
        mInflater=LayoutInflater.from(context);
        mStudent = student;
    }

    @Override
    public int getCount() {
        return mStudent.size();
    }

    @Override
    public Student getItem(int position) {

        return mStudent.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.view_student, parent, false);
            holder = new ViewHolder();
            holder.avatar = (ImageView) view.findViewById(R.id.icon);
            holder.name = (TextView) view.findViewById(R.id.labelName);
            holder.roll = (TextView) view.findViewById(R.id.labelRoll);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        Student student = getItem(position);
        String temp = student.getAvatar();
        if (temp == null) {
            holder.avatar.setImageResource(R.mipmap.ic_launcher);
        } else {


            Bitmap tempImage = Bitmap.createBitmap(BitmapFactory.decodeFile(temp));
            float originalHeight = tempImage.getHeight();
            float originalWidth = tempImage.getWidth();
            Float tempNewHeight = originalHeight / originalWidth * 80;
            int newHeight = tempNewHeight.intValue();
            Bitmap resized = Bitmap.createScaledBitmap(tempImage, 80, newHeight, true);
            holder.avatar.setImageBitmap(resized);
        }


        holder.name.setText(student.getStudentName());

        holder.roll.setText(student.getStudentRoll().toString());

        return view;
    }

    private class ViewHolder {
        public ImageView avatar;
        public TextView name;
        public TextView roll;
    }


}

