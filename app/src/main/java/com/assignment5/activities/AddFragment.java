package com.assignment5.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.assignment5.util.CommunicationChannel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by click on 7/15/15.
 */
public class AddFragment extends android.support.v4.app.Fragment {
    Button imageButton;
    Button addButton;
    TextView rollEditText;
    String picturePath=null;
    TextView nameEditText;
    final int SELECT_FILE = 5;
    ImageView studentImage;
    final int REQUEST_CAMERA = 4;
    final String PICTURE_SELECT_TITLE = "Add Photo!";
    final String PICTURE_SELECT_OPTION_1 = "Take Photo";
    final String PICTURE_SELECT_OPTION_2 = "Choose from Library";
    final String PICTURE_SELECT_OPTION_3 = "Cancel";
    final String STUDENT_LIST = "listStudent";
    final String ADD_FIELDS_NULL="nullFromAddFragment";
    CommunicationChannel comm;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.add_fragment_layout, container, false);
        rollEditText = (EditText) rootView.findViewById(R.id.rollNoEditText);
        studentImage = (ImageView) rootView.findViewById(R.id.studentImage);
        studentImage.setImageResource(R.mipmap.ic_launcher);
        nameEditText = (EditText) rootView.findViewById(R.id.nameEditText);
        imageButton = (Button) rootView.findViewById(R.id.addImageButton);
        //imageButton.setVisibility(View.INVISIBLE);
        addButton = (Button) rootView.findViewById(R.id.addStudentButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String name = nameEditText.getText().toString();
                    Integer roll = Integer.parseInt(rollEditText.getText().toString());
                    nameEditText.setText("");
                    rollEditText.setText("");
                    studentImage.setImageResource(R.mipmap.ic_launcher);
                    Bundle bundle = new Bundle();
                    bundle.putString("transaction", STUDENT_LIST);
                    bundle.putString("name", name);
                    bundle.putInt("roll", roll.intValue());
                    bundle.putString("picturePath", picturePath);
                    picturePath=null;
                    sendMessage(bundle);

                } catch (Exception npe) {
                    Toast.makeText(getActivity(), "Enter Valid Details", Toast.LENGTH_SHORT);
                    sendMessage(new Bundle());
                }

            }
        });
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = {PICTURE_SELECT_OPTION_1, PICTURE_SELECT_OPTION_2,
                        PICTURE_SELECT_OPTION_3};

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(PICTURE_SELECT_TITLE);
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals(PICTURE_SELECT_OPTION_1)) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, REQUEST_CAMERA);
                        } else if (items[item].equals(PICTURE_SELECT_OPTION_2)) {
                            Intent i = new Intent(
                                    Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                            startActivityForResult(i, SELECT_FILE);
                        } else if (items[item].equals(PICTURE_SELECT_OPTION_3)) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        });
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_FILE && resultCode == getActivity().RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
            studentImage.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            // String picturePath contains the path of selected Image
        }
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                picturePath = destination.getAbsolutePath();
                //Intent intent = new Intent(this, MainActivity.class);
                //intent.putExtra("picturePath", picturePath);
                studentImage.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                // ivImage.setImageBitmap(thumbnail);

            }

        }


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof CommunicationChannel) {
            comm = (CommunicationChannel) activity;
        } else {
            throw new ClassCastException();
        }
    }

    public void sendMessage(Bundle bundle) {
        if (bundle.size()==0) {
            bundle.putString("transaction",ADD_FIELDS_NULL);
            comm.setCommunication(bundle);
        }
        else {comm.setCommunication(bundle);}
    }

}
