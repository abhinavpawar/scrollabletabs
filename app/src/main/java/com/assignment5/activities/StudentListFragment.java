package com.assignment5.activities;


import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import com.assignment5.util.CommunicationChannel;
import com.assignment5.util.MyBaseAdapter;
import com.assignment5.util.Student;

import java.util.ArrayList;

/**
 * Created by click on 7/15/15.
 */
public class StudentListFragment extends Fragment {
    final String VIEW_INDIVIDUAL_STUDENT = "viewFragment";
    Parcelable mListInstanceState;
    CommunicationChannel comm;
    ArrayList<Student> students = new ArrayList<Student>();
    MyBaseAdapter adapter;
    ListView listView;
    final String CONTEXTMENU_TITLE = "Select Action";
    final int EDIT = 1;
    final int DELETE = 2;
    final int DELETE_ALL = 3;
    AdapterView.AdapterContextMenuInfo menuInfo;
    final String CONTEXT_ITEM_1 = "Edit";
    final String CONTEXT_ITEM_2 = "Delete";
    final String CONTEXT_ITEM_3 = "Delete All";
    final String EDIT_FRAGMENT_MESSAGE="editFragmentMessage";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.student_list_fragment, container, false);
        adapter = new MyBaseAdapter(getActivity(), students);
        listView = (ListView) view.findViewById(R.id.list);
        listView.setAdapter(adapter);
        registerForContextMenu(listView);
        GridView gridView = (GridView) view.findViewById(R.id.grid);
        gridView.setAdapter(adapter);
        registerForContextMenu(listView);
        //listView.setVisibility(View.INVISIBLE);
        gridView.setVisibility(View.INVISIBLE);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString("picturePath",adapter.getItem(position).getAvatar());
                bundle.putString("name", adapter.getItem(position).getStudentName());
                bundle.putInt("roll", adapter.getItem(position).getStudentRoll());
                bundle.putString("transaction", VIEW_INDIVIDUAL_STUDENT);
                comm.setCommunication(bundle);
            }
        });

        return view;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(CONTEXTMENU_TITLE);
        menu.add(0, 1, 1, CONTEXT_ITEM_1);
        menu.add(0, 2, 2, CONTEXT_ITEM_2);
        menu.add(0, 3, 3, CONTEXT_ITEM_3);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int id = item.getItemId();
        switch (id) {
            case DELETE_ALL: {
                students.clear();
                adapter.notifyDataSetChanged();
                return true;

            }
            case DELETE: {
                Student student=adapter.getItem(menuInfo.position);
                students.remove(student);
                adapter.notifyDataSetChanged();
                return true;
                //break;

            }
            case EDIT: {
                Bundle bundle=new Bundle();
                bundle.putString("name", adapter.getItem(menuInfo.position).getStudentName());
                bundle.putString("picturePath",adapter.getItem(menuInfo.position).getAvatar());
                bundle.putInt("roll", adapter.getItem(menuInfo.position).getStudentRoll());
                bundle.putString("transaction", EDIT_FRAGMENT_MESSAGE);
                comm.setCommunication(bundle);
                return true;
                 //break;
            }

            default:
                return true;

        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof CommunicationChannel) {
            comm = (CommunicationChannel) activity;
        } else {
            throw new ClassCastException();
        }
    }


    public void receiver(Bundle bundle) {
        Integer roll = (Integer) bundle.get("roll");
        String name = bundle.getString("name");
        String picturePath = bundle.getString("picturePath");
        students.add(new Student(roll, name, picturePath));
        adapter.notifyDataSetChanged();
    }

    public void receiverForEditing(Bundle bundle) {
        Integer roll = (Integer) bundle.get("roll");
        String name = bundle.getString("name");
        String picturePath = bundle.getString("picturePath");
        adapter.getItem(menuInfo.position).setStudentName(name);
        adapter.getItem(menuInfo.position).setStudentRoll(roll);
        adapter.getItem(menuInfo.position).setAvatar(picturePath);
        adapter.notifyDataSetChanged();
    }
}
