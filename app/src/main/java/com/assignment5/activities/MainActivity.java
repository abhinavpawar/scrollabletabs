package com.assignment5.activities;

import android.support.v4.app.FragmentManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.PagerTitleStrip;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.view.ViewPager;
import android.view.View;
import com.assignment5.util.CommunicationChannel;
import com.assignment5.util.PagerAdapter;

public class MainActivity extends FragmentActivity implements CommunicationChannel{
final String STUDENT_LIST_FRAGMENT="StudentListFragment";
    ViewPager viewPager;
    final String EDIT_STUDENT="editStudent";
    final String STUDENT_LIST="listStudent";
    final String VIEW_INDIVIDUAL_STUDENT="viewFragment";
    final String EDIT_FRAGMENT_MESSAGE="editFragmentMessage";
    final String ADD_FIELDS_NULL="nullFromAddFragment";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // if(getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.activity_main);
            viewPager = (ViewPager) findViewById(R.id.pager);
            FragmentManager fragmentManager = getSupportFragmentManager();
            PagerAdapter pagerAdapter = new PagerAdapter(fragmentManager);
            viewPager.setAdapter(pagerAdapter);
            PagerTabStrip pagerTabStrip = (PagerTabStrip) findViewById(R.id.title);
            pagerTabStrip.setTextColor(Color.WHITE);
            //pagerTabStrip.setBackgroundColor(Color.DKGRAY);
            pagerTabStrip.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            pagerTabStrip.setTabIndicatorColor(Color.WHITE);




    }



    @Override
    public void setCommunication(Bundle bundle) {
        String operation=bundle.getString("transaction");
        switch(operation){
            case VIEW_INDIVIDUAL_STUDENT:{
                viewPager.setCurrentItem(2);
                ViewFragment viewFragment=(ViewFragment)getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + 2);
                viewFragment.getView().setVisibility(View.VISIBLE);
                viewFragment.receive(bundle);
                viewPager.setCurrentItem(2);
                break;
            }
            case STUDENT_LIST:{
                StudentListFragment studentListFragment=(StudentListFragment)getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + 0);
                studentListFragment.receiver(bundle);
                FragmentManager manager=getSupportFragmentManager();
                FragmentTransaction transaction=manager.beginTransaction();
                transaction.attach(studentListFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                viewPager.setCurrentItem(0);
                break;
            }
            case EDIT_STUDENT:{
                StudentListFragment studentListFragment=(StudentListFragment)getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + 0);
                studentListFragment.receiverForEditing(bundle);
                FragmentManager manager=getSupportFragmentManager();
                FragmentTransaction transaction=manager.beginTransaction();
                transaction.attach(studentListFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                viewPager.setCurrentItem(0);
                break;
            }
            case EDIT_FRAGMENT_MESSAGE:{

                viewPager.setCurrentItem(3);
                EditFragment editFragment=(EditFragment)getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + 3);
                editFragment.getView().setVisibility(View.VISIBLE);
                editFragment.receive(bundle);
                viewPager.setCurrentItem(3);
                break;
            }
            case ADD_FIELDS_NULL:{
                viewPager.setCurrentItem(0);
                break;
            }
            default: viewPager.setCurrentItem(0);
        }


    }
}
