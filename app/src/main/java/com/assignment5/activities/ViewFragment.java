package com.assignment5.activities;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.assignment5.util.Student;

/**
 * Created by click on 7/15/15.
 */
public class ViewFragment extends android.support.v4.app.Fragment {
    ImageView studentIcon;
    Integer rollno;
    Student student;
    TextView roll;
    TextView name;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.view_fragment_layout, container, false);
        rootView.setVisibility(View.INVISIBLE);
        roll = (TextView) rootView.findViewById(R.id.rollView);
        name = (TextView) rootView.findViewById(R.id.nameView);
        studentIcon=(ImageView)rootView.findViewById(R.id.studentIcon);
        return rootView;
    }

    public void receive(Bundle bundle) {

        Integer studentRoll = (Integer) bundle.get("roll");
        String studentName = bundle.getString("name");
        String picturePath=bundle.getString("picturePath");
        roll.setText(studentRoll.toString());
        name.setText(studentName);
        if (picturePath == null) {
            studentIcon.setImageResource(R.mipmap.ic_launcher);
        } else {
            Bitmap bmImg = BitmapFactory.decodeFile(picturePath);
            studentIcon.setImageBitmap(bmImg);
        }
    }
}

