package com.assignment5.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.assignment5.util.CommunicationChannel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by click on 7/17/15.
 */
public class EditFragment extends Fragment {
    CommunicationChannel comm;
    Button saveButton;
    EditText roll;
    EditText name;
    ImageView image;
    final int SELECT_FILE = 5;
    ImageView studentImage;
    final int REQUEST_CAMERA = 4;
    final String PICTURE_SELECT_TITLE = "Add Photo!";
    final String PICTURE_SELECT_OPTION_1 = "Take Photo";
    final String PICTURE_SELECT_OPTION_2 = "Choose from Library";
    final String PICTURE_SELECT_OPTION_3 = "Cancel";


    final String EDIT_STUDENT = "editStudent";
    String picturePath;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.edit_fragment_layout, container, false);
view.setVisibility(View.INVISIBLE);
        roll = (EditText) view.findViewById(R.id.rollNoEditText);
        name = (EditText) view.findViewById(R.id.nameEditText);
        image = (ImageView) view.findViewById(R.id.imageView);
        saveButton = (Button) view.findViewById(R.id.saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer rollString = Integer.parseInt(roll.getText().toString());
                String nameString = name.getText().toString();
                Bundle bundle = new Bundle();
                bundle.putString("name", nameString);
                bundle.putString("picturePath",picturePath);
                bundle.putInt("roll", rollString.intValue());
                bundle.putString("transaction", EDIT_STUDENT);
                send(bundle);
            }
        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = {PICTURE_SELECT_OPTION_1, PICTURE_SELECT_OPTION_2,
                        PICTURE_SELECT_OPTION_3};

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(PICTURE_SELECT_TITLE);
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals(PICTURE_SELECT_OPTION_1)) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, REQUEST_CAMERA);
                        } else if (items[item].equals(PICTURE_SELECT_OPTION_2)) {
                            Intent i = new Intent(
                                    Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                            startActivityForResult(i, SELECT_FILE);
                        } else if (items[item].equals(PICTURE_SELECT_OPTION_3)) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof CommunicationChannel) {
            comm = (CommunicationChannel) activity;
        } else {
            throw new ClassCastException();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_FILE && resultCode == getActivity().RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
            image.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            // String picturePath contains the path of selected Image
        }
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");

                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                picturePath = destination.getAbsolutePath();
                //Intent intent = new Intent(this, MainActivity.class);
                //intent.putExtra("picturePath", picturePath);
                image.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                // ivImage.setImageBitmap(thumbnail);

            }

        }


    }

    public void receive(Bundle bundle) {
        Integer studentRoll = (Integer) bundle.get("roll");
        String studentName = bundle.getString("name");
        picturePath = bundle.getString("picturePath");
        roll.setText(studentRoll.toString());
        name.setText(studentName);
        if (picturePath == null) {
            image.setImageResource(R.mipmap.ic_launcher);
        } else {
            Bitmap bmImg = BitmapFactory.decodeFile(picturePath);
            image.setImageBitmap(bmImg);
        }
    }

    public void send(Bundle bundle) {
        comm.setCommunication(bundle);
    }
}
